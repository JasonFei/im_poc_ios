//
//  ContentView.swift
//  amplify-api-realtime-chat
//
//  Created by 创新中心-iOSDev on 2021/12/22.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
