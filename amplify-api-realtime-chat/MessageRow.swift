//
//  MessageRow.swift
//  AWSChatApp
//
//  Created by 创新中心-iOSDev on 2021/12/22.
//

import SwiftUI

struct MessageRow: View {
    let message:Message
    let isCurrentUser:Bool
    private var iconName:String{
        if let initial  = message.senderName.first{
            return  initial.lowercased()
        }else{
            return "qiestionmark"
        }
    }
    
    private var iconColor :Color{
        if isCurrentUser {
            return
                .blue
        }else{
            return .green
        }
    }
    var body: some View {
        VStack(alignment:.leading){
            HStack(alignment:.top){
                
                Image(systemName: "\(iconName).circle.fill").font(.largeTitle).foregroundColor(iconColor)
                VStack(alignment:.leading)
                {
                    Text(message.senderName).font(.headline)
                    Text(message.body).font(.body)
                }
            }.padding()
            Divider().padding(.leading,16)
        }
    }
}


struct MessageRow_Previews: PreviewProvider {
    static var previews: some View {
        MessageRow(message: Message(senderName: "kilo loco", body: "hello world", creationDate: 0), isCurrentUser: true)
    }
}
