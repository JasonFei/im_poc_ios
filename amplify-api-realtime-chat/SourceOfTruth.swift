//
//  SourceOfTruth.swift
//  amplify-api-realtime-chat
//
//  Created by 创新中心-iOSDev on 2021/12/22.
//
import Amplify
import Foundation
import AppSyncRealTimeClient

class SourceOfTruth:ObservableObject{
    @Published var messages = [Message]()
    func send(_ message:Message){
        Amplify.API.mutate(request: .create(message)){ mutationResult in
            switch mutationResult {
            case .success(let creationResult):
                switch creationResult{
                case .success:
                    print("successfully creat message")
                case .failure(let error):
                    print(error)
                }
            case .failure(let apiError):
                print(apiError)
            }
            
        }
    }
    
    func getMessages(){
        Amplify.API.query(request: .list(Message.self)){ [weak self]   result in
            do{
                let message = try result.get().get()
                DispatchQueue.main.async {
                    self?.messages =  message.sorted(by: {$0.creationDate
                        < $1.creationDate
                    })
                }
            }catch{
                print(error)
            }
        }
    }
    func delete(_ message:Message){
        Amplify.API.mutate(request: .delete(message)){ result in
            print(result)
            
        }
    }
    func deleteAllMessage(){
        self.messages.forEach{message in
            self.delete(message)
            self.messages.removeAll()
        }
    }
    //    var  subscription:GraphQLSubscriptionOperation<Message>?
    //    func observeMessages(){
    //        subscription = Amplify.API.subscribe(request: .subscription(of: Message.self, type: .onCreate
    //                                                                   ),
    //                                             valueListener: { [weak self] subscriptionEvent in
    //            switch subscriptionEvent{
    //            case .connection(let ConnectionState):
    //                print("connection state:",ConnectionState)
    //            case .data(let dateResult):
    //                do {
    //                    let  message = try dateResult.get()
    //                    DispatchQueue.main.async {
    //                        self?.messages.append(message)
    //                    }
    //                } catch {
    //                    print(error)
    //                }
    //            }
    //        }, completionListener: { completion
    //            in
    //            print(completion)
    //        })
    //    }
    
    var  subscription:GraphQLSubscriptionOperation<String>?
    func subscribeMessage(){
        subscription  = Amplify.API.subscribe(request: CustomRequest.subscribeMessage()){ subscriptionEvent in
            switch subscriptionEvent{
            case .connection(let connectionState):
                print("Connection state:\(connectionState)")
                break
            case .data(let dataResult):
                do {
                    let message = try dataResult.get()
                    if let data = message.data(using:.utf8){
                        do {
                            let model:Message = try self.decodeModel(from:data)
                            DispatchQueue.main.async {
                                self.messages.append(model)
                            }
                        } catch {
                            
                        }
                    }
                    
                }catch{
                    print(error)
                }
                
            }
        }completionListener: {completion  in
            print(completion)
        }
    }
    
    func decodeModel<T : Codable>(from data: Data) throws -> T {
        return try JSONDecoder().decode(T.self, from: data)
    }
    
    func SignIn(){
        Amplify.Auth.signIn(username: "wangyanfei@johnsonfitness.com", password: "WQe566766266") { result in
            switch result {
            case .success:
                print("登录成功:\(result)")
            case .failure(let error):
                print("登录error:\(error)")
            }
        }
    }
    
    func checkAuthStatus(){
        Amplify.Auth.fetchAuthSession{ (result) in
            switch result{
            case .success(let authSession):
                print("The current user is signed in:\(authSession)")
                if  authSession.isSignedIn {
                    print("User is signed in")
                } else {
                    print("User is signed out")
                }
            case .failure(let authError):
                print("Failed to fetch the Auth Session",authError)
            }
        }
    }
    
    func getCurrentUser(getUser: @escaping (_ user: AuthUser)->Void) {
        if let user = Amplify.Auth.getCurrentUser() {
            getUser(user)
        }
    }
    
}
struct CustomRequest{
    static func subscribeMessage() -> GraphQLRequest<String> {
        let apiName = "amplifyapirealtimech"
        let operationName = "onCreateMessage"
        let document = """
                subscription MySubscription {
                  onCreateMessage {
                    senderName
                    id
                    body
                    creationDate
                  }
                }
        """
        return GraphQLRequest(apiName: apiName, document: document,responseType: String.self, decodePath: operationName)
    }
}
