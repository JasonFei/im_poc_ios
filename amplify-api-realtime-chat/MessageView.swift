//
//  MessageView.swift
//  AWSChatApp
//
//  Created by 创新中心-iOSDev on 2021/12/22.
//

import SwiftUI
import Amplify
struct MessageView: View {
    @State var text = String()
    @ObservedObject var sot = SourceOfTruth()
    @State var currentUser = String()
    
    init(){
        sot.getMessages()
//      sot.observeMessages()
        sot.subscribeMessage()
        sot.checkAuthStatus()
    }
    
    var body: some View {
        VStack{
            HStack{
                TextField("Enter UserName",text: $currentUser).padding()
                Button {
                    sot.deleteAllMessage()
                    sot.getMessages()
//                    sot.delete(sot.messages)
//                    sot.messages.forEach{ message in
//                        sot.delete(message)
//                    }
                } label: {
                    Text("Clear Message")
                }
//                Button {
//                    sot.SignIn()
//                } label: {
//                    Text("Sign in")
//                }
            }
            ScrollView{
                LazyVStack{
                    ForEach(sot.messages){ message in
                        MessageRow(message:message,isCurrentUser:message.senderName  == currentUser)
                    }
                }
            }
            HStack{
                TextField("Enter message",text: $text)
                Button("Send",action: {
                    didTapSend()
                }).padding().foregroundColor(.white).background(Color.purple)
            }
        }.padding(.horizontal,16)
    }
    func didTapSend(){
        print(text)
        let message = Message(
            senderName : currentUser,
            body:text,
            creationDate:Int(Date().timeIntervalSince1970)
        )
        sot.send(message)
        text.removeAll()
    }
}

struct MessageView_Previews: PreviewProvider {
    static var previews: some View {
        MessageView()
    }
}
