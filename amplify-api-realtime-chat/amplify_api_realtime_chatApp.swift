//
//  amplify_api_realtime_chatApp.swift
//  amplify-api-realtime-chat
//
//  Created by 创新中心-iOSDev on 2021/12/22.
//
import Amplify
import AmplifyPlugins
import SwiftUI
//import AWSAppSync
//import AWSMobileClient
@main
struct amplify_api_realtime_chatApp: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
//    init(){
//        configureAmplify()
//    }
    var body: some Scene {
        WindowGroup {
//            ContentView()
            MessageView()
        }
    }
   
}
class AppDelegate: NSObject, UIApplicationDelegate {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

//        do {
//            try Amplify.add(plugin: AWSCognitoAuthPlugin())
//            try Amplify.add(plugin: AWSAPIPlugin())
//            try Amplify.configure()
//            print("Amplify configured with auth plugin")
//        } catch {
//            print("Failed to initialize Amplify with \(error)")
//        }
        do {
            if let url = Bundle.main.url(forResource: "amplifyconfiguration", withExtension: ".json") {
                let configuration = try AmplifyConfiguration(configurationFile: url)
                Amplify.Logging.logLevel = .verbose
                try Amplify.add(plugin: AWSCognitoAuthPlugin())
                try Amplify.add(plugin: AWSAPIPlugin())
                try Amplify.configure(configuration)
                print("Amplify configured with auth plugin")
            }
        } catch {
            print("Failed to initialize Amplify with \(error)")
        }

        return true
    }
}

func configureAmplify(){
    do{
        let  models = AmplifyModels()
        try  Amplify.add(plugin:AWSAPIPlugin(modelRegistration:models))
        try  Amplify.configure()
        print("Configured amplify")
    }catch{
        print("Couldnt configure amplify",error)
    }
    
}
